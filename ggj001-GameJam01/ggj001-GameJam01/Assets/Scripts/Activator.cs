using System;
using DG.Tweening;
using UnityEngine;

public class Activator : MonoBehaviour
{
    [Header("Scaled Target objects")]
    [SerializeField] private GameObject[] targetObjects; // scaled ones
    
    [Header("Target Objects That is Colored")]
    [SerializeField] private GameObject[] targetColoredObjects; // to be colored

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ThirdPersonMovement>()) ActivateObjects();

        if (other.GetComponent<Npc>()) Deactivate();
    }

    private void ActivateObjects()
    {
        foreach (var target in targetObjects)
        {
            target.SetActive(true);
            target.transform.localScale = Vector3.zero;
            target.transform.DOScale(Vector3.one, 1.5f).SetEase(Ease.OutBack);
        }
    }

    private void Deactivate()
    {
        foreach (var target in targetObjects)
            target.transform.DOScale(Vector3.zero, 1.5f).SetEase(Ease.OutBack)
                .OnComplete(() => target.SetActive(false));
    }
}