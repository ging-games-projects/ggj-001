﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Npc : MonoBehaviour
{

    [SerializeField] private Transform groundPlane;
    private NavMeshAgent agent;
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        NpcBehaviorStarter();
    }

    private void NpcBehaviorStarter()
    {

        StartCoroutine(Behavior());
        IEnumerator Behavior()
        {
            agent.SetDestination(GetRandomPoint());
            yield return new WaitForSeconds(5);

            StartCoroutine(Behavior());
        }
    }

    Vector3 GetRandomPoint()
    {
        Vector3 _randomPoint = new Vector3();
        _randomPoint = new Vector3(Random.Range(-groundPlane.localScale.x, groundPlane.localScale.x), 0, Random.Range(-groundPlane.localScale.z, groundPlane.localScale.z));
        Debug.Log(_randomPoint);
        return _randomPoint * 10;

    }

}